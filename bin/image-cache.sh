#!/bin/bash

mkdir -p output/html/images
mkdir -p output/epub
mkdir -p output/mobi
mkdir -p output/docbook
mkdir -p output/multihtml/images

# section-level images
mkdir -p images
yes|cp -P modules/*/images/*.png images/
yes|cp -P modules/*/images/*.png output/html/images/
yes|cp -P modules/*/images/*.png output/multihtml/images/
yes|cp -P modules/*/images/*.jpg images/
yes|cp -P modules/*/images/*.jpg output/html/images/
yes|cp -P modules/*/images/*.jpg output/multihtml/images/

# chapter-level images
#yes|cp -P modules/*/*/images/*.png images/
#yes|cp -P modules/*/*/images/*.png output/html/images/
#yes|cp -P modules/*/*/images/*.png output/multihtml/images/
#yes|cp -P modules/*/*/images/*.jpg images/
#yes|cp -P modules/*/*/images/*.jpg output/html/images/
#yes|cp -P modules/*/*/images/*.jpg output/multihtml/images/

exit 0
