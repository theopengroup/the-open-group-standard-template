#!/bin/bash
# simple HTML build - independent

bin/image-cache.sh

TOG_DOCNAME=$1
#asciidoctor -v -r asciidoctor-diagram -D output/html -o ${TOG_DOCNAME}.html  _book-html.adoc
asciidoctor --trace -r asciidoctor-multipage -b multipage_html5 -v -D output/multihtml -o ${TOG_DOCNAME}.html  _book-html.adoc
