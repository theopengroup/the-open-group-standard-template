#!/bin/sh
# A simple build script
#
# Usage $0 [clean | html | pdf | epub | all]
#
# Should be run from the top level of the project
#

if [ $# -eq 0 ]
then
    echo "$0 [clean | html | html-multipage | pdf | all]"
    echo "all = html and pdf"
    exit 0
fi
if [ ! -r bin ]
then
    echo "Unable to locate the bin directory, check you are in the top level of the project"
    exit 1
fi

if [ "$TOG_DOCNAME" = "" ]
then
   . ./_vars.sh
   echo "TOG_DOCNAME not set in environment, using setting from _vars.sh"
fi
export TOG_DOCNAME

case $1 in
clean)
  rm -rf output asciidoc_err.txt ;;
html)
  echo "Building html only"
  sh bin/render-html.sh ${TOG_DOCNAME} 2> asciidoc_err.txt
  ;;
epub)
  echo "Building epub only"
  sh bin/render-epub.sh ${TOG_DOCNAME} 2> asciidoc_err.txt
  ;;
# experimental
html-multipage)
# html multipage is experimental and requires some postprocessing
#  echo "Building html multipage only"
  sh bin/render-html-multipage.sh ${TOG_DOCNAME} 2> asciidoc_err.txt
  ;;
pdf)
  echo "Building pdf only"
  sh bin/render-pdf.sh  ${TOG_DOCNAME} 2> asciidoc_err.txt
  ;;
#pdf-a5)
#    echo "Building pdf-a5 only"
#    mv _book-pdf.adoc __book-pdf.adoc
#    cp _book-pdf-a5.adoc _book-pdf.adoc
#    sh bin/render-pdf.sh  ${TOG_DOCNAME}-A5 2> asciidoc_err.txt
#    mv __book-pdf.adoc _book-pdf.adoc
#    ;;
*)
# unnecessary as contained in later scripts
#  echo "Setting up the file tree..."
#  sh bin/image-cache.sh 2> asciidoc_err
  echo "Building html"
  sh bin/render-html.sh ${TOG_DOCNAME}  2> asciidoc_err.txt
  echo "Building pdf"
  sh bin/render-pdf.sh ${TOG_DOCNAME} 2> asciidoc_err.txt
;;
esac
exit 0
