// Note Definitions must be its own chapter per The Open Group Standard template, and so is at Level 3.
// For a guide, this can be moved to a Glossary appendix.
ifeval::["{tog-doctype}" != "Guide"]
[#Definitions]
= Definitions
indexterm:[definitions]

ifeval::["{tog-guidance}" == "true"]
******************
*Guidance*

A standard must have a chapter on terms and definitions.

This should start with the text below.

There is no need for the standard to define a term if the dictionary definition as defined by Merriam-Webster's Collegiate Dictionary suffices. If no terms need to be defined, use the following text:

For the purposes of this standard, all terms are as defined in Merriam-Webster’s Collegiate Dictionary.
******************
endif::[]

For the purposes of this document, the following terms and definitions apply. Merriam-Webster’s Collegiate Dictionary should be referenced for terms not defined in this section.

ifeval::["{tog-guidance}" == "true"]
******************
*Guidance*

Select one of the following formats for definitions:

Variable lists:

If you have few terms you can use the variable list format.

Headings:

For documents with large numbers of terms define them with a heading.

******************
endif::[]

Term::
A brief self-contained definition of the term.

Term 2::
A brief self-contained definition of the term.

== Term 3

A brief self-contained definition of the term.

== Term 4

A brief self-contained definition of the term.

ifeval::["{tog-guidance}" == "true"]
******************
*Guidance*

_Most Words Need No Definition_

There is no need to define a term if the dictionary definition as defined by Merriam-Webster’s Collegiate Dictionary suffices.
If an English-speaking reader (using Merriam-Webster’s Collegiate Dictionary as needed) would interpret a word in its intended sense, there is no need to define it. If Merriam-Webster’s Collegiate Dictionary offers several meanings for a word, and only one of those meanings conveys your intent, be sure that the rest of the sentence drives the reader to that particular meaning.

_Alphabetical Order_

Definitions should normally appear in alphabetical order and the term defined should be written out completely and should not be inverted (e.g., “Application Platform” rather than “Platform, Application”).

_Self-Contained Definitions_

Each definition should be a brief, self-contained description of the term in question and shall not contain any other information, such as requirements, elaborative text, or prescriptions on how the term is to be used.

_Term Not Used in its Own Definition_

A term should not be used in its own definition.

_Cross-References_

Cross-references should occur after the definition. “See:” refers to a term where the desired definition can be found. “See also:” refers to a related term.

_Citing External Definitions_

If an external standard meeting the standards adoption criteria has a meaning suitable for use, we should use the same definition and cite the source.

_Notes on Definitions_

As with all writing, the meaning of each statement should be unambiguous. If multiple readers interpret a statement differently, then it is likely that something should be changed.

_Citing a Referenced Document_

The TOGAF Standard [xref:00-front-matter:references.adoc#C220[C220]] is available on The Open Group website. The Open Group Standards Process [xref:00-front-matter:references.adoc#Ref1[Ref1]] is also available on the website.

******************
endif::[]
// ! Guide
endif::[]
