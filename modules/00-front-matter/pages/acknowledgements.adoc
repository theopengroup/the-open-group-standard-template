// note [[anchor]] format now legacy 
[#Acknowledgements]
== Acknowledgements

ifeval::["{tog-guidance}" == "true"]
******************
*Guidance*

Any acknowledgements should be listed here. If there are none, exclude this section from the build.

Check https://www.opengroup.org/membership-list for affiliations of members of The Open Group. If the affiliation is not a listed member company at the time of publication, use the text "`Invited Expert`".

Individuals named in this section are usually eligible for Contribution Awards &#8211; see https://www.opengroup.org/contribution-awards.

******************
endif::[]

(Please note affiliations were current at the time of approval.)

The Open Group gratefully acknowledges the contribution of the following people in the development of this document:

* TBA

The Open Group gratefully acknowledges the following reviewers who participated in the Company Review of this document:

* TBA
* _list_
